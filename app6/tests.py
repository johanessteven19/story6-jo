from django.test import TestCase, Client
from django.urls import resolve
from .models import Data
from .views import home
from .forms import DataForm
# Create your tests here.

class Story6Test(TestCase):
    def test_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_view_use_correct_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'app6/index.html')
    
    def test_lab_3_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    def test_inside_html(self):
        response=Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, how are you?", response_content)

    def test_model(self):
        Data.objects.create(name ="Budiman")
        counting_all_variable_activity = Data.objects.all().count()
        self.assertEqual(counting_all_variable_activity, 1)

    def test_form_validation_blank_items(self):
        form = DataForm(data={'name':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
    


    
    

